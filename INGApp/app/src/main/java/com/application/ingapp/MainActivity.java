package com.application.ingapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

public class mainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button shoppingButton = (Button) findViewById(R.id.shoppingButton);
        ImageButton img_shoppingButton = (ImageButton) findViewById(R.id.imageButton);

        shoppingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity.this, searchProduct.class);
                //intent.putExtra("key", title);
                mainActivity.this.startActivity(intent);
            }
        });

        img_shoppingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity.this, searchProduct.class);
                //intent.putExtra("key", title);
                mainActivity.this.startActivity(intent);
            }
        });
    }
}
