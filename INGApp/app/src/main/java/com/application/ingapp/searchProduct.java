package com.application.ingapp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

public class searchProduct extends AppCompatActivity {
    static protected String title = "";
    MaterialSearchView searchView;
    ListView listView;
    ArrayList<String> oldQueries = new ArrayList<String>();
    String[] products = {
            "Epiphone Les Paul",
            "Gibson SG",
            "Iphone X",
            "Blackstar ID20 Amp"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_product);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Search Product");
        toolbar.setTitleTextColor(Color.parseColor("#FFFFFF"));

        searchView = (MaterialSearchView) findViewById(R.id.search_view);

        listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter arrayAdapter = new ArrayAdapter(searchProduct.this, android.R.layout.simple_list_item_1, oldQueries);
        listView.setAdapter(arrayAdapter);
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                title = (String)arg0.getAdapter().getItem(position);
                Intent intent = new Intent(searchProduct.this, ShowProduct.class);
                //intent.putExtra("key", title);
                searchProduct.this.startActivity(intent);
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
            }

            @Override
            public void onSearchViewClosed() {
                //if closed search view, listView will return default
                listView = (ListView) findViewById(R.id.listView);
                ArrayAdapter arrayAdapter = new ArrayAdapter(searchProduct.this, android.R.layout.simple_list_item_1, oldQueries);
                listView.setAdapter(arrayAdapter);
            }
        });

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null && !newText.isEmpty()){
                    List<String> productsFound = new ArrayList<String>();
                    for(String product : products){
                        if(product.toLowerCase().contains(newText.toLowerCase()))
                            productsFound.add(product);
                    }

                    ArrayAdapter arrayAdapter = new ArrayAdapter(searchProduct.this, android.R.layout.simple_list_item_1, productsFound );
                    listView.setAdapter(arrayAdapter);
                    int index = 0;
                    boolean exists = false;
                    for(int i = 0; i < oldQueries.size(); i++){
                        if(oldQueries.get(i).contains(newText)) {
                            exists = true;
                            index = i;
                        }
                    }
                    if (!exists)
                        oldQueries.add(0, newText);
                }
                else{
                    //if new text is null or empty
                    //return default
                    ArrayAdapter arrayAdapter = new ArrayAdapter(searchProduct.this, android.R.layout.simple_list_item_1, oldQueries);
                    listView.setAdapter(arrayAdapter);
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        MenuItem item  = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public void closeOptionsMenu() {
        super.closeOptionsMenu();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
