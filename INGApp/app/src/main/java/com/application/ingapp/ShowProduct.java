package com.application.ingapp;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ShowProduct extends AppCompatActivity {

    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product);

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(searchProduct.title);

        ArrayList<GraphView> graphList = new ArrayList<GraphView>();
        GraphView graph = (GraphView) findViewById(R.id.graph);
        for(int i = 0; i < 2; i++, graph = (GraphView) findViewById(R.id.graph2))
        {
            Viewport graphSettings = graph.getViewport();
            //graphSettings.setBackgroundColor(R.color.themeColor);
            /*
            graphSettings.setXAxisBoundsManual(true);
            graphSettings.setYAxisBoundsManual(true);

            int maxX, minX, maxY, minY;


            graphSettings.setMaxX(maxX);
            graphSettings.setMaxY(maxY);
            graphSettings.setMinX(minX);
            graphSettings.setMinY(minY);
    */
            ArrayList<DataPoint> datas;
            LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[]{
                    new DataPoint(1, 2000),
                    new DataPoint(2, 2100),
                    new DataPoint(3, 2440),
                    new DataPoint(4, 2500),
                    new DataPoint(5, 2530),
                    new DataPoint(6, 2530),
                    new DataPoint(7, 2530),
                    new DataPoint(8, 2530),
                    new DataPoint(9, 2530),
                    new DataPoint(10, 2530),
                    new DataPoint(11, 2530),
                    new DataPoint(12, 2530)

            });
            series.setColor(Color.rgb(243, 96, 7));
            series.setDrawDataPoints(true);
            series.setDataPointsRadius(7);
            series.setThickness(8);

            GridLabelRenderer GLR = graph.getGridLabelRenderer();
            GLR.setHorizontalAxisTitle("Ay");
            //GLR.setHorizontalAxisTitleColor(R.color.orange);

            GLR.setVerticalAxisTitle("Fiyat");
            //GLR.setVerticalAxisTitleColor(R.color.orange);

            //title
            graph.setTitle("Fiyat Değişim Tablosu");
            graph.setTitleColor(R.color.darkBlue);
            graph.setTitleTextSize(50);

            graph.addSeries(series);
            graphList.add(graph);
        }

        TextView resultView = (TextView) findViewById(R.id.resultView);
        resultView.setText("Almak istediğiniz ürünün, 14 gün içerisinde %10 indirime girme olasılığı %19.");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }
}
